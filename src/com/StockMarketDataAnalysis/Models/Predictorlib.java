package com.StockMarketDataAnalysis.Models;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.ResilientPropagation;
import org.neuroph.util.TransferFunctionType;
import org.neuroph.core.data.DataSetRow;

public class Predictorlib {
	private ArrayList<Double> rvalues = new ArrayList<Double>();
	public double runNetwork() throws Exception {
        double last = 0;
        Reader rd = new FileReader("open.txt");
        Reader rd1 = new FileReader("close.txt");
        FileWriter r = new FileWriter("stockReturns.txt");
        BufferedWriter bw = new BufferedWriter(r);
        BufferedReader open = new BufferedReader(rd);
        BufferedReader close = new BufferedReader(rd1);
        double lastclose = 0;
	    
        String op ,cl;
        while(((op = open.readLine()) != null) && ((cl = close.readLine()) != null)){
            double x,y;
            x = Double.parseDouble(op);
            y = Double.parseDouble(cl);
            rvalues.add(y-x);
            last = y;            
            String rval = Double.toString(y-x);            
            bw.write(rval);
        }
        MultiLayerPerceptron network = new MultiLayerPerceptron(TransferFunctionType.SIN, 10,25,15,1);
        network.setLearningRule(new ResilientPropagation());
        DataSet trainingSet = new DataSet(10, 1);   
        for(int i = 0; i < rvalues.size(); i++){
            if(i+11 <= rvalues.size()){
                int count = 0;
                double a[] = new double[10];
                double b = 0;
                for(int j = 0; j < a.length;j++){
                    a[j] = rvalues.get(i+j)/100;
            	    if(a[j] > 1){
                        a[j] = 1;
                    }
                    else if(a[j] < -1){
                        a[j] = -1;
                    }
                    if(j == (a.length-1)){ 
                        b = rvalues.get(i+j+1)/100;
                        if(b > 1) b = 1;
                        if(b < -1) b = -1;
                    }
                }	
            trainingSet.addRow(new DataSetRow(a, new double[]{b}));
            }
	    }
        System.out.println("before Training");   
        network.learn(trainingSet);
        System.out.println("after Training");
        System.out.println("Training set"); 
		network.save("network.nnet");
        open.close();
        close.close();
        r.close();
        double out[] = network.getOutput();
        return ((out[0]*100));
	}
}
