package com.StockMarketDataAnalysis.Models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import jsc.distributions.Normal;
import jsc.goodnessfit.*;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;


public class Analyze {


    public Boolean isNormal(double x[]) throws FileNotFoundException, IOException{
        
        
        DescriptiveStatistics ds = new DescriptiveStatistics(x); // for calculating mean and sd of sample data
	
        Normal n = new Normal(ds.getMean(), ds.getStandardDeviation()); // Hypothetical Normal dist. created from sample mean and sd
        
        KolmogorovTest kst = new KolmogorovTest(x, n);
        //System.out.println("d= "+kst.getD()); // D = test statistic
        
        //System.out.println("mean = "+ds.getMean());
        //System.out.println("sd = "+ds.getStandardDeviation());
        
	double temp = cValue(x.length);
        //System.out.println("cvalue = "+temp);
        if(kst.getD() < temp)
        {
            return true;
            
        }
        else
        {
            return false;
        }
     }
    public static double cValue(int n)
    {
        double c;
        c=0;
        switch (n)
        {
            case 4: c = 0.3754; break;
            case 5: c = 0.3427; break;
            case 6: c = 0.3245; break;
            case 7: c = 0.3041; break;
            case 8: c = 0.2875; break;
            case 9: c = 0.2744; break;
            case 10: c = 0.2616; break;
            case 11: c = 0.2506; break;
            case 12: c = 0.2426; break;
            case 13: c = 0.2337; break;
            case 14: c = 0.2257; break;
            case 15: c = 0.2196; break;
            case 16: c = 0.2128; break;
            case 17: c = 0.2071; break;
            case 18: c = 0.2018; break;
            case 19: c = 0.1965; break;
            case 20: c = 0.1920; break;
            case 21: c = 0.1881; break;
            case 22: c = 0.1840; break;
            case 23: c = 0.1798; break;
            case 24: c = 0.1766; break;
            case 25: c = 0.1726; break;
            case 26: c = 0.1699; break;
            case 27: c = 0.1665; break;
            case 28: c = 0.1641; break;
            case 29: c = 0.1614; break;
            case 30: c = 0.1590; break;
            case 31: c = 0.1559; break;
            case 32: c = 0.1542; break;
            case 33: c = 0.1518; break;
            case 34: c = 0.1497; break;
            case 35: c = 0.1478; break;
            case 36: c = 0.1454; break;
            case 37: c = 0.1436; break;
            case 38: c = 0.1421; break;
            case 39: c = 0.1402; break;
            case 40: c = 0.1386; break;
            case 41: c = 0.1373; break;
            case 42: c = 0.1353; break;
            case 43: c = 0.1339; break;
            case 44: c = 0.1322; break;
            case 45: c = 0.1309; break;
            case 46: c = 0.1293; break;
            case 47: c = 0.1282; break;
            case 48: c = 0.1269; break;
            case 49: c = 0.1256; break;
            case 50: c = 0.1246; break;
            
        }
        if(n>50)
        {
            double d = ((0.83+n)/Math.sqrt(n))-0.1;
            c=0.895/d;
        }
        return c;
    }
}
