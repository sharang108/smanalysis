package com.StockMarketDataAnalysis.Models;

/**************************************************************************************************************
	Author 		  : Chinmaya B
	Date Created  : 14-04-2017
	Date Modified : 06-05-2017
	Description   : Neural Networks with Backpropagation; 
	Note		  : Precision 6 decimal points run() is success now implement train() for xor

**************************************************************************************************************/

import java.text.DecimalFormat;
import java.util.concurrent.ConcurrentHashMap;

class Synapse{
	private DecimalFormat newfmt = new DecimalFormat("#.######");
	private float weight,output;
	private int source, destination; //Source and Destination Neuron #

	/*Used to initially assign random weights 
	called in constructor sot that it assigns random weights after creation of every synapse*/
	public void setWeight(){
		double dt = Math.random();
		System.out.println("Random Weights "+dt);
		this.weight = Float.parseFloat(newfmt.format(dt)); 
	}

	/*Assign Weights after changing them with delw*/
	public void setWeight(float weight){
			this.weight = Float.parseFloat(newfmt.format(weight));
	}

	/*Constructor to create Synapse between source and destination with default values*/
	Synapse(int src, int dest){
		this.source = src;
		this.destination = dest;
		setWeight();
	}

	public float getWeight(){
		return this.weight;
	}

	public int getSource(){
		return this.source;
	}

	public int getDestination(){
		return this.destination;
	}

	public float getOutput(){
		return output;
	}

	public void setOutput(float out){
		this.output = out;
	}
}

class Neuron{
	private int sno; //serial number for every neuron, calc outputs;
	private float input, prevlayersum, delta; //prevlayersum is sum of the outputs from previous layer, delta(error for each neuron)
	private DecimalFormat newfmt = new DecimalFormat("#.######");
	private static DecimalFormat newfmt1 = new DecimalFormat("#.######"); //just to make logistic and Derivative static
	private ConcurrentHashMap<String, Synapse>synapses = new ConcurrentHashMap<String, Synapse>();

	
	/*Just create neuron and assign Serial number*/
	Neuron(int sno){
		this.sno = sno;
	}

	/* Keys to the Synapse would be (Source<space>Destination) so synapse from neuron 0 to neuron 2 will have key 0 2 */
	public Synapse getSynapse(int destination){
		return (Synapse) synapses.get(this.sno+" "+destination);

	}

	public void setSynapse(Synapse s, int destination){
		String key = this.sno+" "+destination;
		synapses.put(key, s);
	}

	public void setInput(float ip){
		input = Float.parseFloat(newfmt.format(ip));
	}

	public void incrementInput(float ip){
		input += Float.parseFloat(newfmt.format(ip));
	}

	/*Implements Logistic Function for output calculation*/
	public static float logistic(double val){
		return Float.parseFloat(newfmt1.format (1 / (1 + Math.pow(Math.E, -val))));
	}

	/*Use following method to calculate Weighted Sum of inputs
	calcOutput Stores outputs in Synapses*/
	public float calcOutput(int destination){
		Synapse s = this.getSynapse(destination);
		float out = this.input * s.getWeight();
		s.setOutput(out);
		
		return Float.parseFloat(newfmt.format(out));
	}

	/*If option = 1 use logistic function
		 option = 2 use tan sigmoid function 
		 keeping void for now (returns final output for given input Synapse)
		 output for each outgoing synapse is calculated like this;
	*/
	public float applyActivation(int option){
		double x = Double.parseDouble(new Float(this.input).toString());
		float out = 0;
		switch(option){
			case 1 : 	
				System.out.println("from calcOutput "+logistic(x));
				out = logistic(x);
			break;
			
			case 2 : 
				out = Float.parseFloat(newfmt.format(Math.tanh(x)));
			break;
		}
		return out;
	}
	
	public static float derivativeActivation(float input, int option){
		double x = Double.parseDouble(new Float(input).toString());	
		float out = 0;
		switch(option){
			case 1 :
				out = (logistic(x) * (1 - logistic(x)));
			break;
			case 2 :
				x = (1 - Math.pow(Math.tanh(x), 2.0));
				out = Float.parseFloat(newfmt1.format(x)); 
			break;
		}
		return out;
	}

	public float getfinalOutput(){
		return applyActivation(1);
	}
	
	public int getSno(){
		return this.sno;
	}

	public void setDelta(float del){
		this.delta = del;
	}

	public float getDelta(){
		return Float.parseFloat(newfmt.format(this.delta));
	}
}

class NeuralNetwork {
	private int iterations; //hlayer for synapses
	private float epoch, minerror,learning_rate, error; // try learning rate with 0.01
	private Neuron layerI[], layerH[], layerO[];
	
	/*Create Neurons(0 Indexed neurons) for all Layers */
	public void createNeurons(){
		for(int i = 0; i < layerI.length; i++){
			layerI[i] = new Neuron(i);
		}
		
		for(int i = 0; i < layerH.length; i++){
			layerH[i] = new Neuron(i + layerI.length);
		}

		for(int i = 0; i < layerO.length; i++){
			layerO[i] = new Neuron(i + layerI.length + layerH.length);
		}
	}

	/*Connects all the neurons in layer1 to neurons in layer2 with Synapse
	and stores Synapse in Source Neuron to recall them later in future*/
	public void connectLayers(Neuron[] layer1, Neuron[] layer2){
		for(int i = 0; i < layer1.length; i++){
			for(int j = 0; j < layer2.length; j++){
				
				layer1[i].setSynapse(new Synapse(layer1[i].getSno(), layer2[j].getSno()), layer2[j].getSno());

			}
		}
	}

	NeuralNetwork(float epoch, float minerror, float learning_rate){
		layerI = new Neuron[2];
		layerH = new Neuron[3];
		layerO = new Neuron[1];
		this.epoch = epoch;
		this.minerror = minerror;
		this.learning_rate = learning_rate;
		createNeurons();
		connectLayers(layerI, layerH);
		connectLayers(layerH, layerO);
	}

	NeuralNetwork(int ilayer, int hlayer, int olayer, float epoch, float minerror, float learning_rate){
		layerI = new Neuron[ilayer];
		layerH = new Neuron[hlayer];
		layerO = new Neuron[olayer];
		this.epoch = epoch;
		this.minerror = minerror;
		this.learning_rate = learning_rate;
		createNeurons();
		connectLayers(layerI, layerH);
		connectLayers(layerH, layerO);
	}

	public float run(float input[]){
		iterations++;
		System.out.println("-------------------------------------------------Iteration "+iterations);		
		
		System.out.println("--------------ILayer------------\n");		
		if(iterations == 1)
		for(int i = 0; i < layerI.length; i++){
				layerI[i].setInput(input[i]);
				System.out.println("Setting input "+input[i]);
		}
		
		System.out.println("--------------ILayer------------\n");

		
		System.out.println("--------------HLayer------------\n");	

		/*Weighted sum calculation*/
		for(int i = 0; i < layerI.length; i++){
			for(int j = 0; j < layerH.length; j++){
				layerH[j].incrementInput(layerI[i].calcOutput(layerH[j].getSno()));
			}
		}
		/*Final Input to Hlayers*/
		for(int j = 0; j < layerH.length; j++){
			layerH[j].setInput(layerH[j].getfinalOutput());
		}


		System.out.println("--------------HLayer------------\n");		

		
		System.out.println("--------------OLayer------------\n");		
		
		for(int i = 0; i < layerH.length; i++){
			for(int j = 0; j < layerO.length; j++){
				layerO[j].incrementInput(layerH[i].calcOutput(layerO[j].getSno()));
			}
		}

		System.out.println("--------------OLayer------------\n");		
		
		System.out.println("output "+layerO[0].getfinalOutput()+" 	for Iteration "+iterations);
		return layerO[0].getfinalOutput();
	}

	public void trainBackProp(float[] input, float actual_value){
		float netoutput, error;
		float herror[] = new float[layerH.length];
		do{
			netoutput = run(input);
			error = (actual_value - netoutput);
			
			for(int i = 0; i < layerO.length; i++){
				layerO[i].setDelta(Neuron.derivativeActivation(netoutput, 1) * error);
			}

			//Hidden-Output Synapse Weight Changes
			for(int i = 0; i < layerH.length; i++){
				for(int j = 0; j < layerO.length; j++){
					Synapse s = layerH[i].getSynapse(layerO[j].getSno()); 
					System.out.println("Old Weight "+s.getWeight());
					s.setWeight((s.getWeight() + (learning_rate * s.getOutput() * layerO[j].getDelta()))); 
					System.out.println("New Weight "+s.getWeight());
				}
			}
			
			//Input-Hidden Synapse Weight Changes 
			for(int i = 0; i < layerH.length; i++){
				for(int j = 0; j < layerO.length; j++){
					Synapse s = layerH[i].getSynapse(layerO[j].getSno());
					layerH[i].setDelta((Neuron.derivativeActivation(s.getOutput(), 1) * s.getWeight() * layerO[j].getDelta()));
				}
			}

			//Assign new weights to Input-Hidden Synapses
			for(int i = 0; i < layerI.length; i++){
				for(int j = 0; j < layerH.length; j++){
					Synapse s = layerI[i].getSynapse(layerH[j].getSno());
					System.out.println("Old Weight "+s.getWeight());
					s.setWeight((s.getWeight() + (learning_rate * s.getOutput() * layerH[j].getDelta())));
					System.out.println("New Weight "+s.getWeight());
				}
			}
			
		}while(iterations <= epoch && (error) >= minerror);
		
	}

	public void trainRProp(float[] input, float actual_value){

	}
}

public class Predictor{
	static float ar;
	static float [] br;
	
	
	public static void main(String args[]){
		NeuralNetwork nn = new NeuralNetwork(50, 0.010000f, 0.4f);
		br = new float[] {0,0};
		nn.trainBackProp(br, 1);
		System.out.println("Displaying Run out\t"+ar);
	}
}