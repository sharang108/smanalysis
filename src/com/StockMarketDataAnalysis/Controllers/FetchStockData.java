package com.StockMarketDataAnalysis.Controllers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

@WebServlet({ "/FetchStockData", "/fetchData" })
public class FetchStockData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public FetchStockData() {
        super();
    }
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try{
			String str = request.getParameter("Sname");
            FileWriter wr = new FileWriter("close.txt");
            BufferedWriter bw = new BufferedWriter(wr);
            FileWriter wr1 = new FileWriter("open.txt");
            BufferedWriter bw1 = new BufferedWriter(wr1);

            Calendar from = Calendar.getInstance();
            Calendar to = Calendar.getInstance();
            from.add(Calendar.YEAR, -1); // from 1 years ago

            Stock stk = YahooFinance.get(str);
            List<HistoricalQuote> stkHistQuotes = stk.getHistory(from, to, Interval.DAILY);
            for (ListIterator<HistoricalQuote> it = stkHistQuotes.listIterator(); it.hasNext(); ) 
            {
                HistoricalQuote t = it.next();
                bw.write(""+t.getClose());
                bw.newLine();
                bw1.write(""+t.getOpen());
                bw1.newLine();
            }
            bw.close();
            bw1.close();
            request.setAttribute("count", new Integer(stkHistQuotes.size()));
            PullData pd = new PullData();
            pd.service(request, response);      
			}
			catch(Exception e){
				RequestDispatcher rd = request.getRequestDispatcher("error.html");
				rd.forward(request, response);
			}
		}
	}
