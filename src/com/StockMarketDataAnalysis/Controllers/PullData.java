package com.StockMarketDataAnalysis.Controllers;

/**************************************************************************************************************
Author 		  : 
Date Created  : 30-04-2017
Date Modified : 30-04-2017
Description   : Use Analyze and Predictor on the generated file from fetchData, shows results on 
				displayresults.html/jsp for displaying final results after analysis and prediction.
Note		  : Should generate plots as well or call the plotter API for generating plot image.
**************************************************************************************************************/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.StockMarketDataAnalysis.Models.Analyze;
import com.StockMarketDataAnalysis.Models.Predictorlib;

@WebServlet("/Pulldata")
public class PullData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PullData() {
        super();
    }

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try (PrintWriter out = response.getWriter()) {
            int count1 = (Integer) request.getAttribute("count");
            int count = 0;
            double close[] = new double [count1]; 
            double open[] = new double [count1];
            double rvalue[] = new double[count1];
            String line, line1;
            
            Reader rd = new FileReader("close.txt");
            BufferedReader br = new BufferedReader(rd);
            Reader rd1 = new FileReader("open.txt");
            BufferedReader br1 = new BufferedReader(rd1);
            
            while((line = br.readLine()) != null){
			close[count] = Double.parseDouble(line);
			//System.out.println("line is\t"+x[count]);								
			count++;
			}
            count = 0;
            while((line1 = br1.readLine()) != null){
			open[count] = Double.parseDouble(line1);
			//System.out.println("line is\t"+x[count]);								
			count++;
			}
            
            for (int i = 0; i < count1; i++) {
                rvalue[i] = close[i] - open[i];
            }
            
            Analyze az = new Analyze();
            boolean inc = az.isNormal(close);
            boolean ino = az.isNormal(open);
            boolean inr = az.isNormal(rvalue);
            Predictorlib p = new Predictorlib();
            p.runNetwork();
            RequestDispatcher dsp = request.getRequestDispatcher("output.jsp");
            dsp.forward(request,response);
		}
		catch(Exception e){
			RequestDispatcher rd = request.getRequestDispatcher("error.html");
			rd.forward(request, response);
        }
	}

}
