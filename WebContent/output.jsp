<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.StockMarketDataAnalysis.Models.*" %>
<!Doctype html>
<html>
    <head> 
        <title>Output Stock Market Data Analysis</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="custom.css"></script>
    </head>
    <body>
        <div class ="jumbotron">
                <h1 align="center" id="banner">Stock Market Analyzer</h1>
        </div>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading"><a href ="index.html"> <span class="glyphicon glyphicon-home"></span></a> <h4 algin="center">Stock Analysis Report</h4> </div>
                <div class="panel-body"> 
                <h3>Predicted Change in stock price
                <% Predictorlib pl = new Predictorlib();%>
                <%=pl.runNetwork() %>
                </h3>
                </div>
             </div>    
        </div>
    </body>
</html>